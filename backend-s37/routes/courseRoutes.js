const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth")

// router.post("/", (req, res) => {
// 	const userData = auth.decode(req.headers.authorization);
// 	console.log(userData)

// 	if(userData.isAdmin === true ){
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// 	} else {
// 	  return res.send({auth : "failed"});
// 	}

// })

 router.post("/", auth.verify, (req, res) => {

 	const data = {
 		course: req.body,
 		isAdmin: auth.decode(req.headers.authorization).isAdmin
 	}

 	courseController.addCourse(data).then(resultFromController => res.send(resultFromController))
 });


router.get("/all", (req, res) => {
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController));

 });


router.get("/active", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});


router.get("/:courseId/details", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:id", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/archive", auth.verify, (req, res) => {
	courseController.updateisActive(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;