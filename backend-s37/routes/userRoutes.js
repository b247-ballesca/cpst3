const express = require("express");
const router = express.Router();
const auth = require("../auth")
const userController = require("../controllers/userController")

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData)

	userController.getProfile(userData).then(resultFromController => res.send(resultFromController));
});

// router.post("/enroll", (req, res) => {

// 	let data = {
// 		userId : req.body.userId,
// 		courseId : req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController => res.send(resultFromController));
// })

router.post("/enroll", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData)

	let data = {
		userId : userData.id,
		courseId : req.body.courseId
	}

	console.log(data)
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;

